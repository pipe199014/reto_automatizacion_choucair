#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Formulario Block Validation
  El usuario debe poder ingresar al formulario los datos requeridos.
  Cada campo del formulario realiza validaciones de obligatoriedad,
  longitud y formato, el sistema debe presentar las validaciones respectivas
  para cada campo a traves un globo informativo.

  @CasoExitoso
  Scenario: Diligenciamiento exitoso del formulario Block Validation,
    no se presenta ningún mensaje de validación.

    Given Autentico en colorlib con usuario "demo" y la clave "demo"
    And Ingreso a la funcionalidad Form Validation
    When Diligencio Formulario Block Validation
     | Required | Email           | password | confirm_password |    date    |   url                | DigitsOnly | Range | PolicyAccept |
     | Valor1   | felipe@mail.com | valor1   | valor1           | 02022018   | http://www.valor1.com| 1234       | 8     |  1           |
   Then Verifico ingreso exitoso

  @CasoAlterno
  Scenario: Diligenciamiento con errores del formulario Block Validation,
    se presentó error informativo indicando error en el diligenciamiento de algunos campos.

    Given Autentico en colorlib con usuario "demo" y la clave "demo"
    And Ingreso a la funcionalidad Form Validation
    When Diligencio Formulario Block Validation
  | Required | Email           | password | confirm_password |    date    |   url                | DigitsOnly | Range | PolicyAccept |
  |          | felipe@mail.com | valor1   | valor1           | 02022018   | http://www.valor1.com| 1234       | 8     |  1           |
      Then Verificar que se presenten mensajes de validacion.
    
   