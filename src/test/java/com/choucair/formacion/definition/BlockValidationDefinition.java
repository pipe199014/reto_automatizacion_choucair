package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.BlockValidationSteps;
import com.choucair.formacion.steps.ColorlibFormValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class BlockValidationDefinition 
{
	@Steps
	BlockValidationSteps BlockValidationSteps;
	@Steps
	ColorlibFormValidationSteps colorlibFormValidationSteps;

	@Given("^Autentico en colorlib con usuario \"([^\"]*)\" y la clave \"([^\"]*)\"$")
	public void autentico_en_colorlib_con_usuario_y_la_clave(String strUsuario, String strPass) throws Throwable {
		BlockValidationSteps.login_colorlib(strUsuario, strPass);
	}

	@Given("^Ingreso a la funcionalidad Form Validation$")
	public void ingreso_a_la_funcionalidad_Form_Validation() throws Throwable {
		BlockValidationSteps.ingresar_form_validation();
	}

	@When("^Diligencio Formulario Block Validation$")
	public void diligencio_Formulario_Block_Validation(DataTable dtDatosForms) throws Throwable {
		List<List<String>> data = dtDatosForms.raw();

		for (int i = 1; i < data.size(); i++) {
			colorlibFormValidationSteps.diligenciar_popup_datos_tabla(data, i);
		try 
		{
			Thread.sleep(5000);
		}
		catch(InterruptedException e) {}
		}
	}

	@Then("^Verifico ingreso exitoso$")
	public void verifico_ingreso_exitoso() {
		colorlibFormValidationSteps.verificar_ingreso_datos_formulario_exitoso();
	}
	
	@Then("^Verificar que se presenten mensajes de validacion\\.$")
	public void verificar_que_se_presenten_mensajes_de_validacion() {
		colorlibFormValidationSteps.verificar_ingreso_datos_formulario_con_errores();
	}

}
