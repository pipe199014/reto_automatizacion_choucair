package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;


public class ColorlibMenuPage extends PageObject{

		// Menu Forms
		@FindBy(xpath = "//*[@id=\"menu\"]/li[6]/a/span[1]")
		public WebElement menuForms;
		// Submenu form general
			@FindBy(xpath = "//*[@id=\"menu\"]/li[6]/ul/li[1]/a")
			public WebElement menuFormGeneral;
		// Submenu Form Validation
		@FindBy(xpath = "//*[@id=\"menu\"]/li[6]/ul/li[2]/a")
		public WebElement menuFormValidation;
		// Label BlockValidation
		@FindBy(xpath = "//*[@id=\"content\"]/div/div/div[2]/div/div/header/h5")
		public WebElement lblBlockValidation;
		
		public void menuFormValidation() 
		{
			menuForms.click();
			menuFormValidation.click();
			String strMensaje = lblBlockValidation.getText();
			assertThat(strMensaje, containsString("Block Validation"));
		}
}
