package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import java.util.Properties;

import java.awt.List;
import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.annotations.findby.How;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class ColorlibFormValidationPage extends PageObject {
	static Properties propiedades;
	// Campo required
	// @FindBy(how = How.ID, id = "~r")
	@FindBy(xpath = "//*[@id=\"required2\"]")
	public WebElementFacade txtRequired;
	// Campo email
	@FindBy(xpath = "//*[@id=\"email2\"]")
	public WebElementFacade txtEmail;
	// Campo password
	@FindBy(xpath = "//*[@id=\"password2\"]")
	public WebElementFacade txtPass;
	// Campo confirmar password
	@FindBy(xpath = "//*[@id=\"confirm_password2\"]")
	public WebElementFacade txtPass2;
	// Campo fecha
	@FindBy(xpath = "//*[@id=\"date2\"]")
	public WebElementFacade txtDate;
	// Url
	@FindBy(xpath = "//*[@id=\"url2\"]")
	public WebElementFacade txtUrl;
	// Solo digitos
	@FindBy(xpath = "//*[@id=\"digits\"]")
	public WebElementFacade txtDigitsOnly;
	// Range
	@FindBy(xpath = "//*[@id=\"range\"]")
	public WebElementFacade txtRange;
	// chkPolicyAccept
	@FindBy(xpath = "//*[@id=\"agree2\"]")
	public WebElement chkPolicyAccept;
	// Boton validar
	@FindBy(xpath = "//*[@id=\"block-validate\"]/div[10]/input")
	public WebElementFacade btnValidate;

	////////////////////////////////// Mensajes de
	////////////////////////////////// validacion///////////////////////////////////////////////////

	// Validacion exitosa
	@FindBy(className = "form-group has-success")
	public WebElementFacade msjValidate;

	///////////////////////////////////////////////// Propiedades de los
	///////////////////////////////////////////////// objetos/////////////////////////////////////////////

	public void Required(String datoPrueba) {
		txtRequired.click();
		txtRequired.clear();
		txtRequired.sendKeys(datoPrueba);
	}

	public void email(String datoPrueba) {
		txtEmail.click();
		txtEmail.clear();
		txtEmail.sendKeys(datoPrueba);
	}

	public void password(String datoPrueba) {
		txtPass.click();
		txtPass.clear();
		txtPass.sendKeys(datoPrueba);
	}

	public void confirm_password(String datoPrueba) {
		txtPass2.click();
		txtPass2.clear();
		txtPass2.sendKeys(datoPrueba);
	}

	public void date(String datoPrueba) {
		txtDate.click();
		txtDate.sendKeys(datoPrueba);
	}

	public void url(String datoPrueba) {
		txtUrl.click();
		txtUrl.clear();
		txtUrl.sendKeys(datoPrueba);
	}

	public void DigitsOnly(String datoPrueba) {
		txtDigitsOnly.click();
		txtDigitsOnly.clear();
		txtDigitsOnly.sendKeys(datoPrueba);
	}

	public void Range(String datoPrueba) {
		txtRange.click();
		txtRange.clear();
		txtRange.sendKeys(datoPrueba);
	}

	public void PolicyAccept(String datoPrueba) {

		// if (datoPrueba == "1")
		chkPolicyAccept.click();

	}

	public void validate() {
		btnValidate.click();

	}

	/////////////////////////////////// Campos de
	/////////////////////////////////// validación///////////////////////////////////////

	public void form_sin_errores() {
		// By.cssSelector("class~has-error");
		int lista = this.getDriver().findElements(By.cssSelector("div[class~='has-error']")).size();
		boolean valido = lista == 0;
		System.out.println(valido);
		System.out.println(lista);
		assert (valido);
	}

	public void form_con_errores() {
		int lista = this.getDriver().findElements(By.cssSelector("div[class~='has-error']")).size();
		boolean valido = lista > 0;
		System.out.println(valido);
		System.out.println(lista);
		assert (valido);
	}
}
